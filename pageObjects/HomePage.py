from selenium.webdriver.common.by import By


class Home_page:

    def __init__(self, driver):
        self.driver = driver

    CSAOndemand = (By.XPATH, "(//a[text()='CSA OnDemand™'])[2]")

    def ClickCSAOnDemand(self):
        return self.driver.find_element(*Home_page.CSAOndemand)

    ProductName = (By.XPATH, "//div[@class='jumbotron']//h2")

    def veifyProductBannerName(self, expected):
        banner_name = self.driver.find_element(*Home_page.ProductName).getText()
        assert banner_name == expected

    Bucket_Home_Link1_H3 = (By.XPATH, "(//section[@class='buckets-home']//h3)[1]")

    def verifyBucket_Home_Link1_H3_Text(self, expected):
        link_text_1 = self.driver.find_element(*Home_page.Bucket_Home_Link1_H3).getText()
        assert link_text_1 == expected

    Bucket_Home_Link2_H3 = (By.XPATH, "(//section[@class='buckets-home']//h3)[2]")

    def verifyBucket_Home_Link2_H3_Text(self, expected):
        link_text_2 = self.driver.find_element(*Home_page.Bucket_Home_Link2_H3).getText()
        assert link_text_2 == expected

    Bucket_Home_Link3_H3 = (By.XPATH, "(//section[@class='buckets-home']//h3)[3]")

    def verifyBucket_Home_Link3_H3_Text(self, expected):
        link_text_3 = self.driver.find_element(*Home_page.Bucket_Home_Link3_H3).getText()
        assert link_text_3 == expected


