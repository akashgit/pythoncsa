from pageObjects.HomePage import Home_page
from utilities.BaseClass import BaseClass


class TestHome(BaseClass):

    def test_e2e_HomePage(self, setup):
        log = self.getLogger()
        log.info("Browser & URL Invoked")

        hp = Home_page(self.driver)
        hp.ClickCSAOnDemand().click()
        log.info("CSA On demand clicked")

